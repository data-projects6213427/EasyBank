package org.fasttrackit;

import javax.swing.*;
import java.io.InputStream;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("  - = FastTrack Bank = - ");
        System.out.println();
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);

        showWelocmingScreen();

        String userselection = keyboard.nextLine();
        if (userselection.equals("1")) {
            getAskUserDetails(keyboard);
        }
        if (userselection.equals("2")){
            System.out.print("Please enter your login id: ");
            String loginId = keyboard.nextLine();
            System.out.print("Please enter your password: ");
            String password = keyboard.nextLine();

            User bankUser = fetchUser();
//if bank user is autenticated with login and password...
//display user and password
            if (bankUser.isAutenticated(loginId, password)) {
                String bankUserEmail = bankUser.getEmail();
                String userPhoneNr = bankUser.getPhone();
                System.out.println(bankUserEmail + " / " + userPhoneNr);
            }
            else {
                System.out.println("Username or password is incorrect.");
            }
        }

        Result askUserDetails = getAskUserDetails(keyboard);
        User bankUser = new User(askUserDetails.user(), askUserDetails.password(), askUserDetails.name(), askUserDetails.surname());

        String bankUserEmail = bankUser.getEmail();
        String bankPhoneNr = bankUser.getPhone();
        System.out.println(bankUserEmail + " / " + bankPhoneNr);

    }

    private static void showWelocmingScreen() {
        System.out.println("Welcome to the new FastTrack Bank application");
        System.out.println();
        System.out.println("What operation would you like to do?");
        System.out.println("1. Register new account");
        System.out.println("2. Login with existing account");
    }

    private static Result getAskUserDetails(Scanner keyboard) {
        System.out.print("Please enter user: ");
        String user = keyboard.nextLine();
        System.out.print("Please enter password: ");
        String password = keyboard.nextLine();

        System.out.print("Please enter Name: ");
        String name = keyboard.nextLine();
        System.out.print("Please enter surname: ");
        String surname = keyboard.nextLine();

        System.out.print("Please enter email: ");
        String email = keyboard.nextLine();
        System.out.print("Please enter Phone nr: ");
        String phoneNr = keyboard.nextLine();
        Result askUserDetails = new Result(user, password, name, surname);
        return askUserDetails;
    }
    private static User fetchUser() {
        String user = "rami.sh";
        String password = "12345";
        String name = "Sharaiyri";
        String surname = "Rami";
        String email = "ramy_ab@yahoo.com";
        String phoneNr = "0745700837";
        User bankUser = new User(user, password, name, surname);
        bankUser.setEmail(email);
        bankUser.setPhone(phoneNr);
        return bankUser;
    }

    private record Result(String user, String password, String name, String surname) {
    }
}