package org.fasttrackit;

public class Account {
    private final String iban;

    public Account(String iban, String currency) {
        this.iban = iban;
        this.currency = currency;
    }
    public String getIban() {
        return iban;
    }

    public String getCurrency() {
        return currency;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    private final String currency;
    private double balance;
    private String name;

    }
}
