package org.fasttrackit;

//user -> (user / password / Name / Surname / email / Phone) (/)
public class User {
    private final Credentials credentials;
    private final Account account;
    private final String Name;
    private final String Surname;
    private final String email;
    private final String Phone;

    public User(String userName, String password, String Name, String Surname) {
        this.credentials = new Credentials(userName, password);
        this.account = new Account( iban: "RO55BTRLRONCRT01100101", currency: "RON" );
        this.Name = Name;
        this.Surname = Surname;
        this.email = "";
        this.Phone = "";
    }

    public boolean isAutenticated(String loginId, String password) {
        if (loginId.equals(this.credentials.getLoginId()) && password.equals(this.credentials.getPassword())){
            return true;
        }
        return false;
    }
    public boolean isAutenticated(Credentials credentials) {
        if (this.credentials.equals(credentials)){
            return true;
        }
        return false;

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return Phone;
    }


    }

    public Account getAccount() {
        return account;
    }



